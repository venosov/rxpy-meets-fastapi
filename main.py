from fastapi import FastAPI
import asyncio
import functools
import rx
from rx.disposable import Disposable
from rx import operators as op
import aiohttp

app = FastAPI()


def from_aiter(iterable, loop):
    # noinspection PyTypeChecker,PyUnusedLocal
    def on_subscribe(observer, scheduler):
        async def _aio_sub():
            try:
                async for i in iterable:
                    observer.on_next(i)
                loop.call_soon(
                    observer.on_completed)
            except Exception as e:
                loop.call_soon(
                    functools.partial(observer.on_error, e))

        task = asyncio.ensure_future(_aio_sub(), loop=loop)
        return Disposable(lambda: task.cancel())

    return rx.create(on_subscribe)


async def get():
    async with aiohttp.ClientSession() as session:
        async with session.get('http://httpbin.org/get') as resp:
            yield await resp.json()


@app.get("/")
async def read_root():
    loop = asyncio.get_event_loop()
    return await from_aiter(get(), loop).pipe(
        op.map(lambda d: d['url'])
    )
